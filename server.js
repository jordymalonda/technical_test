const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const db = require('./models');
const productRoutes = require('./app/routes/productRoutes');
const userRoutes = require('./app/routes/userRoutes');

const PORT = process.env.PORT | 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/vnd.api+json'}));

app.use(express.static('app/public'));

productRoutes(app,db);
userRoutes(app,db);

db.sequelize.sync().then(function(){
    app.listen(PORT, function(){
        console.log(`Running On Port ${PORT}`)
    });
});
