module.exports = function (app, db){
    var jwt = require('jsonwebtoken');
    var bcrypt = require('bcryptjs');
    var config = require('../../config');

    //Authentication API
    app.get('/auth' , function(req, res){
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            
            console.log(decoded);
            // res.status(200).send(decoded);
            db.User.findById(decoded.id).then((result,err) =>{
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!result) return res.status(404).send("No user found.");
                
                res.status(200).send(result);
            });
        });
    })

    // Create Token API
    app.post('/register' , function(req,res){
        bcrypt.hash(req.body.password, 5, function( err, bcryptedPassword) {
            db.User.find({
                username: req.body.username,
                password: bcryptedPassword
            }).then(function(result, err){
                if (err) return res.status(500).send("There was a problem adding the information to the database.");
                // create a token
                var token = jwt.sign({ id: result.dataValues.id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token });
            });
        });
    });

    // Create User API
    app.post('/signup' , function(req,res){
        bcrypt.hash(req.body.password, 5, function( err, bcryptedPassword) {
            db.User.create({
                username: req.body.username,
                password: bcryptedPassword,
            }).then((result, err)=> {
                if (err) return res.status(500).send("There was a problem adding information to the database");
                res.status(200).send({
                    message : 'User Created'
                })
            })
         });
    });


    // User Login Authentication API
    app.post('/login' , function(req,res){
        const condition = {
            where : {
                username: req.body.username
            }
        }
        db.User.find(condition).then(function(result, err){
            var hash = result.dataValues.password;
            bcrypt.compare(req.body.password, hash, function(err, doesMatch){
            if (doesMatch){
                return res.status(200).send({ auth: true, message: "You're Logged in" });
            }else{
                res.status(404).send({ auth: false, message: "There was a problem about your credential" });
            }
            });
        });
    });

    app.delete('/user/delete/:id' , function(req, res){
        db.User.destroy({
            where:{
                id: req.params.id
            }
        }).then(function(result){
            res.json(result);
        });
    });
}