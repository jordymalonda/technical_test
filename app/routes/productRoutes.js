module.exports = function (app, db){
    var jwt = require('jsonwebtoken');
    var bcrypt = require('bcryptjs');
    var config = require('../../config');
    
    //Create Product Function
    app.post('/product/new' , function(req, res){
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            
            console.log(decoded);
            // res.status(200).send(decoded);
            db.User.findById(decoded.id).then((result,err) =>{
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!result) return res.status(404).send("No user found.");
                
                db.Product.create({
                    name: req.body.name,
                    code: req.body.code,
                    category: req.body.category
                }).then(function(result){
                    res.send({
                        status: 200,
                        message: 'Product berhasil di insert',
                        data: result
                    })
                });
            });
        });
    })

    // app.post('/prod', function(req,res){
    //     db.Product.create({
    //         name: req.body.name,
    //         code: req.body.code,
    //         categories: {
    //             name: req.body.category
    //         }            
    //     }, {include:Category}).then(function(result){
    //         res.send({
    //             status: 200,
    //             message: 'Product berhasil di insert',
    //             data: result
    //         })
    //     });
    // })

    // Read All Product Function
    app.get('/product/all' , function(req, res){
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            
            console.log(decoded);
            // res.status(200).send(decoded);
            db.User.findById(decoded.id).then((result,err) =>{
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!result) return res.status(404).send("No user found.");
                
                db.Product.findAll({}).then(function(result, err){
                    if (err) return res.status(500).send("There was a problem adding the information to the database.");
                    res.status(200).send(result);
                })
            });
        });
    })
    
    // Update Product by Id
    app.put('/product/update/:id' , function(req, res){
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            
            console.log(decoded);
            // res.status(200).send(decoded);
            db.User.findById(decoded.id).then((result,err) =>{
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!result) return res.status(404).send("No user found.");
                
                db.Product.update({
                    name: req.body.name
                }, {
                    where: {
                        id: req.params.id
                    }
                }).then(function(result){
                    res.json(result)
                });
            });
        });
    })

    // Delete Product by Id
    app.delete('/product/delete/:id' , function(req, res){
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            
            console.log(decoded);
            // res.status(200).send(decoded);
            db.User.findById(decoded.id).then((result,err) =>{
                if (err) return res.status(500).send("There was a problem finding the user.");
                if (!result) return res.status(404).send("No user found.");
                
                db.Product.destroy({
                    where:{
                        id: req.params.id
                    }
                }).then(function(result){
                    res.json(result);
                });
            });
        });
    })

}