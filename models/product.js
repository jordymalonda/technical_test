'use strict';
module.exports = (sequelize, DataTypes) => {
  var Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    code: DataTypes.STRING,
    category: DataTypes.STRING,
  }, {});
  Product.associate = function(models) {

  };
  return Product;
};